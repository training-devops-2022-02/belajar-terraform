# Belajar Terraform

Membuat resource di cloud platform (misalnya GCP) dengan menggunakan konsep Infrastructure as Code.

1. Cek instalasi Terraform

    ```
    terraform --version
    ```

2. Siapkan Google Cloud Project

    * Project ID : `training-devops-365802`

3. Buat file konfigurasi terraform, yaitu `main.tf`

4. Inisialisasi folder project

    ```
    terraform init
    ```

5. Validasi source code

    ```
    terraform validate
    ```

6. Apply

    ```
    terraform apply
    ```

7. Mendapatkan value untuk konfigurasi GCP resources

    * Daftar region : `gcloud compute regions list | grep asia`
    * Daftar bootdisk untuk VM : `gcloud compute images list | grep ubuntu`

8. Menghapus resources

    ```
    terraform destroy
    ```


# Referensi #

* [Membuat VM yang bisa diakses dengan SSH](https://binx.io/2022/01/07/how-to-create-a-vm-with-ssh-enabled-on-gcp/)